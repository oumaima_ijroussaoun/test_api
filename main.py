from fastapi import FastAPI
from sqlalchemy import orm
from routers import include_route
from routers import test
from fastapi.middleware.cors import CORSMiddleware








app=FastAPI(prefix='/')


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allow_headers=["*"],
)
# port_app(app)

include_route(app)
